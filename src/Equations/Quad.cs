﻿using AurelienRibon.TweenEngine;

namespace AurelienRibon.TweenEngine.Equations
{
    public abstract class Quad : TweenEquation
    {
        public class QuadIn : Quad
        {
            public override float Compute(float t)
            {
                return t * t;
            }

            public override string ToString()
            {
                return "Quad.In";
            }
        }

        public class QuadOut : Quad
        {
            public override float Compute(float t)
            {
                return -t * (t - 2);
            }

            public override string ToString()
            {
                return "Quad.Out";
            }
        }

        public class QuadInOut : Quad
        {
            public override float Compute(float t)
            {
                if((t *= 2) < 1)
                {
                    return 0.5f * t * t;
                }

                return -0.5f * ((--t) * (t - 2) - 1);
            }

            public override string ToString()
            {
                return "Quad.InOut";
            }
        }

        public static readonly Quad.QuadIn IN = new Quad.QuadIn();
        public static readonly Quad.QuadOut OUT = new Quad.QuadOut();
        public static readonly Quad.QuadInOut INOUT = new Quad.QuadInOut();
    }
}
