﻿using System;

namespace AurelienRibon.TweenEngine.Equations
{
    public abstract class Expo : TweenEquation
    {
        public sealed class ExpoIn : Expo
        {
            public override float Compute(float t)
            {
                return (t == 0) ? 0 : (float)Math.Pow(2, 10 * (t - 1));
            }

            public override string ToString()
            {
                return "Expo.In";
            }
        }

        public sealed class ExpoOut : Expo
        {
            public override float Compute(float t)
            {
                return (t == 1) ? 1 : -(float)Math.Pow(2, -10 * t) + 1;
            }

            public override string ToString()
            {
                return "Expo.Out";
            }
        }

        public sealed class ExpoInOut : Expo
        {
            public override float Compute(float t)
            {
                if(t == 0)
                {
                    return 0;
                }

                if(t == 1)
                {
                    return 1;
                }

                if((t *= 2) < 1)
                {
                    return 0.5f * (float)Math.Pow(2, 10 * (t - 1));
                }

                return 0.5f * (-(float)Math.Pow(2, -10 * --t) + 2);
            }

            public override string ToString()
            {
                return "Expo.InOut";
            }
        }

        public static readonly Expo.ExpoIn In = new Expo.ExpoIn();
        public static readonly Expo.ExpoOut Out = new Expo.ExpoOut();
        public static readonly Expo.ExpoInOut InOut = new Expo.ExpoInOut();
    }
}
