﻿namespace AurelienRibon.TweenEngine.Equations
{
    public abstract class Back : TweenEquation
    {
        public sealed class BackIn : Back
        {
            public override float Compute(float t)
            {
                float s = _paramS;
                return t * t * ((s + 1) * t - s);
            }

            public override string ToString()
            {
                return "Back.In";
            }
        }

        public sealed class BackOut : Back
        {
            public override float Compute(float t)
            {
                float s = _paramS;
                return (t -= 1) * t * ((s + 1) * t + s) + 1;
            }

            public override string ToString()
            {
                return "Back.Out";
            }
        }

        public sealed class BackInOut : Back
        {
            public override float Compute(float t)
            {
                float s = _paramS;
                if((t *= 2) < 1)
                {
                    return 0.5f * (t * t * (((s *= (1.525f)) + 1) * t - s));
                }
                return 0.5f * ((t -= 2) * t * (((s *= (1.525f)) + 1) * t + s) + 2);
            }

            public override string ToString()
            {
                return "Back.InOut";
            }
        }

        public static readonly Back.BackIn In = new Back.BackIn();
        public static readonly Back.BackOut Out = new Back.BackOut();
        public static readonly Back.BackInOut InOut = new Back.BackInOut();

        protected float _paramS = 1.70158f;

        public Back s(float s)
        {
            _paramS = s;
            return this;
        }
    }
}
