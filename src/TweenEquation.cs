namespace AurelienRibon.TweenEngine
{
	public abstract class TweenEquation 
	{
		public abstract float Compute(float t);
		
		public bool IsValueOf(string str) 
		{
			return str == ToString();
		}
	}
}