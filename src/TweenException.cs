﻿using System;

namespace AurelienRibon.TweenEngine
{
    public class TweenException : Exception
    {
        public TweenException(string message) : base(message)
        {

        }
    }
}
