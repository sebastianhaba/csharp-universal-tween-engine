namespace AurelienRibon.TweenEngine
{
	public abstract class TweenCallback 
	{
		public const int Begin = 0x01;
		public const int Start = 0x02;
		public const int End = 0x04;
		public const int Complete = 0x08;
		public const int BackBegin = 0x10;
		public const int BackStart = 0x20;
		public const int BackEnd = 0x40;
		public const int BackComplete = 0x80;
		public const int AnyForward = 0x0F;
		public const int AnyBackward = 0xF0;
		public const int Any = 0xFF;

		public abstract void onEvent(int type, BaseTween source);
	}
}