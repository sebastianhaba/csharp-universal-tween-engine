﻿namespace AurelienRibon.TweenEngine.Equations
{
    public abstract class Linear : TweenEquation
    {
        public sealed class LinearInOut : Linear
        {
            public override float Compute(float t)
            {
                return t;
            }

            public override string ToString()
            {
                return "Linear.InOut";
            }
        }

        public static readonly Linear.LinearInOut InOut = new Linear.LinearInOut();
    }
}
