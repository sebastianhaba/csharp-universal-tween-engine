﻿namespace AurelienRibon.TweenEngine.Equations
{
    public abstract class Quint : TweenEquation
    {
        public sealed class QuintIn : Quint
        {
            public override float Compute(float t)
            {
                return t * t * t * t * t;
            }

            public override string ToString()
            {
                return "Quint.In";
            }
        }

        public sealed class QuintOut : Quint
        {
            public override float Compute(float t)
            {
                return (t -= 1) * t * t * t * t + 1;
            }

            public override string ToString()
            {
                return "Quint.Out";
            }
        }

        public sealed class QuintInOut : Quint
        {
            public override float Compute(float t)
            {
                if((t *= 2) < 1)
                {
                    return 0.5f * t * t * t * t * t;
                }

                return 0.5f * ((t -= 2) * t * t * t * t + 2);
            }

            public override string ToString()
            {
                return "Quint.InOut";
            }
        }

        public static readonly Quint.QuintIn In = new Quint.QuintIn();
        public static readonly Quint.QuintOut Out = new Quint.QuintOut();
        public static readonly Quint.QuintInOut InOut = new Quint.QuintInOut();
    }
}
