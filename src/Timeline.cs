﻿using System;
using System.Collections.Generic;

namespace AurelienRibon.TweenEngine
{
    public sealed class Timeline : BaseTween
    {
        private enum Modes { Sequence, Parallel }

        private class TimelineCallback : Pool<Timeline>.ICallback
        {
            public void onPool(Timeline obj)
            {
                obj.Reset();
            }

            public void onUnPool(Timeline obj)
            {
                obj.Reset();
            }
        }

        private class PoolTimeline : Pool<Timeline>
        {
            public PoolTimeline(int initCapacity, ICallback callback) : base(initCapacity, callback)
            {

            }

            protected override Timeline Create()
            {
                return new Timeline();
            }
        }

        private static readonly TimelineCallback _poolCallback = new TimelineCallback();

        private static readonly PoolTimeline _pool = new PoolTimeline(10, _poolCallback);

        private Timeline()
        {
            Children = new List<BaseTween>(10);
            Reset();
        }

        public static Timeline CreateSequence()
        {
            Timeline tl = _pool.Get();
            tl.Setup(Modes.Sequence);
            return tl;
        }

        public static Timeline CreateParallel()
        {
            Timeline tl = _pool.Get();
            tl.Setup(Modes.Parallel);
            return tl;
        }

        public Timeline BeginParallel()
        {
            if(IsBuilt)
            {
                throw new TweenException("You can't push anything to a timeline once it is started");
            }

            Timeline tl = _pool.Get();
            tl.ParentTimeline = CurrentTimeline;
            tl.Mode = Modes.Parallel;
            CurrentTimeline.Children.Add(tl);
            CurrentTimeline = tl;
            return this;
        }

        public Timeline BeginSequence()
        {
            if(IsBuilt)
            {
                throw new TweenException("You can't push anything to a timeline once it is started");
            }

            Timeline tl = _pool.Get();
            tl.ParentTimeline = CurrentTimeline;
            tl.Mode = Modes.Sequence;
            CurrentTimeline.Children.Add(tl);
            CurrentTimeline = tl;
            return this;
        }

        public override BaseTween Build()
        {
            if(IsBuilt)
            {
                return this;
            }

            Duration = 0;

            for(int i = 0; i < Children.Count; i++)
            {
                BaseTween obj = Children[i];

                if(obj.RepeatCount < 0)
                {
                    throw new TweenException("You can't push an object with infinite repetitions in a timeline");
                }

                obj.Build();

                switch(Mode)
                {
                    case Modes.Sequence:
                        float tDelay = Duration;
                        Duration += obj.FullDuration;
                        obj.DelayInSeconds += tDelay;
                        break;

                    case Modes.Parallel:
                        Duration = Math.Max(Duration, obj.FullDuration);
                        break;
                }
            }

            IsBuilt = true;
            return this;
        }

        public Timeline End()
        {
            if(IsBuilt)
            {
                throw new TweenException("You can't push anything to a timeline once it is started");
            }

            if(CurrentTimeline == this)
            {
                throw new TweenException("Nothing to end...");
            }

            CurrentTimeline = CurrentTimeline.ParentTimeline;
            return this;
        }

        public override void Free()
        {
            for(int i = Children.Count - 1; i >= 0; i--)
            {
                BaseTween obj = Children[i];
                Children.RemoveAt(i);
                obj.Free();
            }

            _pool.Free(this);
        }

        public IList<BaseTween> GetChildren()
        {
            if(IsBuilt)
            {
                return ((List<BaseTween>)CurrentTimeline.Children).AsReadOnly();
            }
            else
            {
                return CurrentTimeline.Children;
            }
        }

        public Timeline Push(Tween tween)
        {
            if(IsBuilt)
            {
                throw new TweenException("You can't push anything to a timeline once it is started");
            }
            CurrentTimeline.Children.Add(tween);
            return this;
        }

        public Timeline Push(Timeline timeline)
        {
            if(IsBuilt)
            {
                throw new TweenException("You can't push anything to a timeline once it is started");
            }

            if(timeline.CurrentTimeline != timeline)
            {
                throw new TweenException("You forgot to call a few 'End()' statements in your pushed timeline");
            }

            timeline.ParentTimeline = CurrentTimeline;
            CurrentTimeline.Children.Add(timeline);
            return this;
        }

        public Timeline PushPause(float time)
        {
            if(IsBuilt)
            {
                throw new TweenException("You can't push anything to a timeline once it is started");
            }
            CurrentTimeline.Children.Add(Tween.Mark().Delay(time));
            return this;
        }

        public override BaseTween Start()
        {
            base.Start();

            for(int i = 0; i < Children.Count; i++)
            {
                BaseTween obj = Children[i];
                obj.Start();
            }

            return this;
        }

        protected override void Reset()
        {
            base.Reset();

            Children.Clear();
            CurrentTimeline = ParentTimeline = null;

            IsBuilt = false;
        }

        protected override void UpdateOverride(int step, int lastStep, bool isIterationStep, float delta)
        {
            if(!isIterationStep && step > lastStep)
            {
                float dt = IsReverse(lastStep) ? -delta - 1 : delta + 1;
                for(int i = 0, n = Children.Count; i < n; i++)
                {
                    Children[i].Update(dt);
                }
                return;
            }

            if(!isIterationStep && step < lastStep)
            {
                float dt = IsReverse(lastStep) ? -delta - 1 : delta + 1;
                for(int i = Children.Count - 1; i >= 0; i--)
                {
                    Children[i].Update(dt);
                }
                return;
            }

            if(step > lastStep)
            {
                if(IsReverse(step))
                {
                    ForceEndValues();
                    for(int i = 0, n = Children.Count; i < n; i++)
                    {
                        Children[i].Update(delta);
                    }
                }
                else
                {
                    ForceStartValues();
                    for(int i = 0, n = Children.Count; i < n; i++)
                    {
                        Children[i].Update(delta);
                    }
                }

            }
            else if(step < lastStep)
            {
                if(IsReverse(step))
                {
                    ForceStartValues();
                    for(int i = Children.Count - 1; i >= 0; i--)
                    {
                        Children[i].Update(delta);
                    }
                }
                else
                {
                    ForceEndValues();
                    for(int i = Children.Count - 1; i >= 0; i--)
                    {
                        Children[i].Update(delta);
                    }
                }

            }
            else
            {
                float dt = IsReverse(step) ? -delta : delta;
                if(delta >= 0)
                {
                    for(int i = 0, n = Children.Count; i < n; i++)
                    {
                        Children[i].Update(dt);
                    }
                }
                else
                {
                    for(int i = Children.Count - 1; i >= 0; i--)
                    {
                        Children[i].Update(dt);
                    }
                }
            }
        }

        private void Setup(Modes mode)
        {
            this.Mode = mode;
            this.CurrentTimeline = this;
        }

        internal override bool ContainsTarget(object target)
        {
            for(int i = 0, n = Children.Count; i < n; i++)
            {
                BaseTween obj = Children[i];
                if(obj.ContainsTarget(target))
                {
                    return true;
                }
            }
            return false;
        }

        internal override bool ContainsTarget(object target, int tweenType)
        {
            for(int i = 0, n = Children.Count; i < n; i++)
            {
                BaseTween obj = Children[i];
                if(obj.ContainsTarget(target, tweenType))
                {
                    return true;
                }
            }
            return false;
        }

        internal override void ForceStartValues()
        {
            for(int i = Children.Count - 1; i >= 0; i--)
            {
                BaseTween obj = Children[i];
                obj.ForceToStart();
            }
        }

        internal override void ForceEndValues()
        {
            for(int i = 0, n = Children.Count; i < n; i++)
            {
                BaseTween obj = Children[i];
                obj.ForceToEnd(Duration);
            }
        }

        public static int PoolSize
        {
            get
            {
                return _pool.Size;
            }
        }

        private IList<BaseTween> Children
        {
            get;
            set;
        }

        private Timeline CurrentTimeline
        {
            get;
            set;
        }

        private bool IsBuilt
        {
            get;
            set;
        }

        private Modes Mode
        {
            get;
            set;
        }

        private Timeline ParentTimeline
        {
            get;
            set;
        }
    }
}
