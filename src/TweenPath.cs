using System;

namespace AurelienRibon.TweenEngine
{
	public abstract class TweenPath 
	{
		public abstract float Compute(float t, float[] points, int pointsCnt);
	}
}

