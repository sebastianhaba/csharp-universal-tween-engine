﻿namespace AurelienRibon.TweenEngine.Equations
{
    public abstract class Cubic : TweenEquation
    {
        public sealed class CubicIn : Cubic
        {
            public override float Compute(float t)
            {
                return t * t * t;
            }

            public override string ToString()
            {
                return "Cubic.In";
            }
        }

        public sealed class CubicOut : Cubic
        {
            public override float Compute(float t)
            {
                return (t -= 1) * t * t + 1;
            }

            public override string ToString()
            {
                return "Cubic.Out";
            }
        }

        public sealed class CubicInOut : Cubic
        {
            public override float Compute(float t)
            {
                if((t *= 2) < 1)
                {
                    return 0.5f * t * t * t;
                }
                return 0.5f * ((t -= 2) * t * t + 2);
            }

            public override string ToString()
            {
                return "Cubin.InOut";
            }
        }

        public static readonly Cubic.CubicIn In = new Cubic.CubicIn();
        public static readonly Cubic.CubicOut Out = new Cubic.CubicOut();
        public static readonly Cubic.CubicInOut InOut = new Cubic.CubicInOut();
    }
}
